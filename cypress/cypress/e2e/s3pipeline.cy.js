describe('template spec', () => {
  it('Check if static website is served', () => {
    cy.visit('http://www.miroteststatichostingbucket.s3-website.eu-central-1.amazonaws.com/')
    cy.contains('h1', 'Hello. I am served by S3')
  })
})