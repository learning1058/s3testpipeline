# S3 backend
terraform {
  backend "s3" {
    bucket = "miroslav-terraform-backend"
    key = "terraform2/statefile"
    region = "eu-central-1"
  }
}

provider "aws" {
    region = "eu-central-1"
}

resource "aws_s3_bucket" "bucket-1" {
  bucket = "www.${var.bucket_name}"
}
data "aws_s3_bucket" "selected-bucket" {
  bucket = aws_s3_bucket.bucket-1.bucket
}

# ACL
resource "aws_s3_bucket_acl" "bucket-acl" {
  bucket = data.aws_s3_bucket.selected-bucket.id
  acl    = "public-read"
}

# Versioning
resource "aws_s3_bucket_versioning" "versioning_example" {
  bucket = data.aws_s3_bucket.selected-bucket.id
  versioning_configuration {
    status = "Enabled"
  }
}

# CORS
resource "aws_s3_bucket_cors_configuration" "example" {
  bucket = data.aws_s3_bucket.selected-bucket.id
cors_rule {
    allowed_headers = ["Authorization", "Content-Length"]
    allowed_methods = ["GET", "POST"]
    allowed_origins = ["https://www.${var.domain_name}"]
    max_age_seconds = 3000
  }
}

# Bucket policy
resource "aws_s3_bucket_policy" "bucket-policy" {
  bucket = data.aws_s3_bucket.selected-bucket.id
  policy = data.aws_iam_policy_document.iam-policy-1.json
}
data "aws_iam_policy_document" "iam-policy-1" {
  statement {
    sid    = "1"
    effect = "Allow"
    actions = ["S3:GetObject"]
    resources = [
        "arn:aws:s3:::www.miroteststatichostingbucket",
        "arn:aws:s3:::www.miroteststatichostingbucket/*"
    ]
principals {
      type        = "*"
      identifiers = ["*"]
    }
  }
}

# Website configuration
resource "aws_s3_bucket_website_configuration" "website-config" {
  bucket = data.aws_s3_bucket.selected-bucket.id
index_document {
    suffix = "index.html"
  }
# error_document {
#     key = "404.jpeg"
#   }
# IF you want to use the routing rule
# routing_rule {
#     condition {
#       key_prefix_equals = "/abc"
#     }
#     redirect {
#       replace_key_prefix_with = "comming-soon.jpeg"
#     }
#   }
}

# Object upload
resource "aws_s3_object" "object-upload-html" {
    for_each        = fileset("../html", "*.html")
    bucket          = data.aws_s3_bucket.selected-bucket.id
    key             = each.value
    source          = "../html/${each.value}"
    content_type    = "text/html"
    etag            = filemd5("../html/${each.value}")
    acl             = "public-read"
}
# resource "aws_s3_object" "object-upload-jpg" {
#     for_each        = fileset("uploads/", "*.jpeg")
#     bucket          = data.aws_s3_bucket.static-website-bucket.id
#     key             = each.value
#     source          = "uploads/${each.value}"
#     content_type    = "image/jpeg"
#     etag            = filemd5("uploads/${each.value}")
#     acl             = "public-read"
# }