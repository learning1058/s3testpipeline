variable "bucket_name" {
  type = string
  default = "miroteststatichostingbucket"
}

variable "domain_name" {
    type = string
    default = "miroteststatichostingbucket.s3-website.eu-central-1.amazonaws.com"
}

variable "region" {
    type = string
    default = "eu-central-1"
}
